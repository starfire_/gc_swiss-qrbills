#!/usr/bin/env python3
# -*- coding: utf-8 -*-

try:
    import sys
    from qrbill import QRBill
    from datetime import datetime
except ImportError as import_error:
    print("Problems importing modules:")
    print(import_error)
    sys.exit(2)

class gcsqrbGenerateBill:
    def __init__(self,
                 account=None,
                 creditor_name=None,
                 creditor_street=None,
                 creditor_city=None):
        self.account = account
        self.crd_name = creditor_name
        self.crd_street = creditor_street
        self.crd_city = creditor_city

    def create_bill(self,
                    amount=None,
                    use_amount=True,
                    debtor_name=None,
                    debtor_street=None,
                    debtor_city=None,
                    add_info=None):
        c_postalcode,c_city = self.crd_city.split(' ', 1)
        d_postalcode,d_city = debtor_city.split(' ', 1)
        if use_amount:
            self.qrBill = QRBill(
                account=self.account,
                creditor={'name' : self.crd_name, 'street' : self.crd_street, 'pcode' : c_postalcode, 'city' : c_city},
                amount=amount,
                debtor={'name' : debtor_name, 'street' : debtor_street, 'pcode' : d_postalcode, 'city' : d_city},
                additional_information=add_info,
                language='de'
            )
        else:
            self.qrBill = QRBill(
                account=self.account,
                creditor={'name' : self.crd_name, 'street' : self.crd_street, 'pcode' : c_postalcode, 'city' : c_city},
                debtor={'name' : debtor_name, 'street' : debtor_street, 'pcode' : d_postalcode, 'city' : d_city},
                additional_information=add_info,
                language='de'
            )

        self.bill_filename=datetime.today().strftime('%Y-%m-%d') + "_" + debtor_name + "_" + add_info + ".svg"

    def render_bill(self):
        self.qrBill.as_svg(self.bill_filename)
