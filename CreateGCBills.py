#!/usr/bin/env python3
# -*- coding: utf-8 -*-

try:
    import sys
    import argparse
    import yaml
    import os.path
    from gcsqrbGnuCash import *
    from gcsqrbGenerateBill import *
    from gcsqrbExceptions import gcsqrbNoConfig, gcsqrbInvalidConfig, gcsqrbNoGCDatafile
except ImportError as import_error:
    print("Problems importing modules:")
    print(import_error)
    sys.exit(2)


def create_all_bills(invoices=None,
                     crd_IBAN=None,
                     use_amount=True,
                     crd_name1=None,
                     crd_name2=None,
                     crd_street=None,
                     crd_city=None):
    for billID, invoice in enumerate(invoices):
        amount = eval(str(invoice.GetTotal()))
        crd_name = crd_name1
        if crd_name2 is not None:
            crd_name = crd_name + "\n" + crd_name2
        newBill = gcsqrbGenerateBill(crd_IBAN, crd_name, crd_street, crd_city)
        newBill.create_bill(str(amount), use_amount, invoice.GetOwner().GetName(), invoice.GetOwner().GetAddr().GetAddr1(),
                            invoice.GetOwner().GetAddr().GetAddr2(), invoice.GetID())
        newBill.render_bill()

def get_configuration():
    argParser = argparse.ArgumentParser(description="Create Swiss QR bills from open GnuCash invoices")
    argParser.add_argument("-c", "--config", help="YAML configuration file", default="config.yaml")
    args = argParser.parse_args()
    config_file = args.config
    if not os.path.isfile(config_file):
        raise gcsqrbNoConfig(config_file)
    with open(config_file, "r") as cfp:
        config = yaml.safe_load(cfp)
    return config

def check_configuration(config=None):
    if config is None:
        raise gcsqrbNoConfig("<unknown>")

    gc_data_file = file_config["gnucash"]
    if not os.path.isfile(gc_data_file):
        raise gcsqrbNoGCDatafile(gc_data_file)

    crd_iban = file_config.get("iban")
    crd_name = file_config.get("name")
    crd_2ndname = file_config.get("2nd_name")
    crd_street = file_config.get("street")
    crd_city = file_config.get("city")
    use_amount = file_config.get("useAmount")

    if crd_iban is None:
        raise gcsqrbInvalidConfig("The 'iban' entry is missing")
    if crd_name is None:
        raise gcsqrbInvalidConfig("The 'name' entry is missing")
    if crd_street is None:
        raise gcsqrbInvalidConfig("The 'street' entry is missing")
    if crd_city is None:
        raise gcsqrbInvalidConfig("The 'city' entry is missing")
    if use_amount is None:
        raise gcsqrbInvalidConfig("The 'useAmount' entry is missing")


if __name__ == "__main__":
    file_config = get_configuration()
    check_configuration(file_config)
    gncBook = gcsqrbGnuCash(file_config["gnucash"])
    gncBook.open_book()
    invoices = gncBook.get_active_invoices()
    if "2nd_name" in file_config:
        sec_name=file_config["2nd_name"]
    else:
        sec_name=None
    create_all_bills(invoices, file_config["iban"], file_config["useAmount"], file_config["name"], sec_name, file_config["street"], file_config["city"])
