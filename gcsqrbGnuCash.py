#!/usr/bin/env python3
# -*- coding: utf-8 -*-

try:
    import sys
    import gnucash
    from gnucash import SessionOpenMode
    import gcsqrbExceptions
    from gcsqrbExceptions import gcsqrbInvalidParameter
    from gncinvoicefkt import *
except ImportError as import_error:
    print("Problems importing modules:")
    print(import_error)
    sys.exit(2)


class gcsqrbGnuCash:
    def __init__(self, gc_book_name=None):
        if gc_book_name is None:
            raise gcsqrbInvalidParameter(gc_book_name)
        self.book_name = gc_book_name
        self.book = None

    def open_book(self):
        try:
            session = gnucash.Session(
                self.book_name,
                SessionOpenMode.SESSION_READ_ONLY)
        except Exception as exception:
            print("Cannot open book: " + self.book_name)
            print(exception)
            return 2
        self.book = session.book
        return 0

    def get_active_invoices(self):
        return get_all_invoices(self.book, 0, 1)
