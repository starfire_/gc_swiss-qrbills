#!/usr/bin/env python3
# -*- coding: utf-8 -*-

class gcsqrbInvalidParameter(Exception):
    def __init__(self, parameter):
        self.msg = "Parameter " + parameter + " is invalid"

class gcsqrbNoConfig(Exception):
    def __init__(self, config_filename):
        self.msg = "The configuration file " + config_filename + " does not exist"

class gcsqrbInvalidConfig(Exception):
    def __init__(self, reason):
        self.msg = "The configuration is invalid; reason: " + reason

class gcsqrbNoGCDatafile(Exception):
    def __init__(self, gc_datafile_name):
        self.msg = "The GnuCash datafile " + gc_datafile_name + " could not be found"
