#
# Small script to setup the correct PYTHONPATH environment variable
# Do not execute, but "source" this file
export PYTHONPATH=$PYTHONPATH:/usr/lib/python3/dist-packages
