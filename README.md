# gc_swiss-qrbills

A Python script to create swiss qrbills of all open invoices of a GnuCash book

## Description
As of October 1st 2022, the old orange and red *Einzahlungsschein*s are no longer valid in Switzerland. Instead the new QR Bill has to be used.

This Python script creates these new qrbills for all open incoices of a GnuCash book.

## Prerequisites
gc_swiss-qrbills needs the following programs or scripts present on the system:
* [swiss-qr-bill](https://github.com/claudep/swiss-qr-bill), the Python script that creates the actual qrbill

## Installation
To be written

## Usage
To be written

## Support
To be written

## Contributing
To be written

## Authors and acknowledgment
The following people have been worked and/or contributed to this project:
* Andreas Tscharner <andy@stupidmail.ch>, Maintainer

## License
gc_swss-qrbills is licenced under the GNU General Public License V3
